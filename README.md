R parallel
===========

Ce projet présente l'utilisation des dockers pour la simulation d'un cluster de calcul R (3 machines). Il est basé sur ce projet [ci](https://github.com/FvD/ralparque/blob/master/README.md).

Les images utilisées se basent sur les dockers `rocker/rstudio` et `rocker/r-ver:4.0.1`. Ces images sont disponibles sur Docker Hub à cette [adresse](https://hub.docker.com/repository/docker/gnathan/rmaster).

Le projet comporte deux dossiers (master et nodes) et un fichier docker-compose.

Master
----------

Ce dossier comporte le fichier `Dockerfile` ayant servir à la création de l'image `gnathan/rmaster` et d'un script R `master.R` qui indique l'utilisation de calculs sur le cluster R.
L'image `gnathan/rmaster` est celui à partir duquel le master sera mis en service. Cette image contient un serveur rstudio.

Pour générer l'image `rmaster`, il faut suivre les étapes suivantes à partir du terminal, après avoir cloner ce repertoire.
```
    $ cd master
    $ docker build -t rmaster:v0.0.1 .
    $ docker run --name master -i rmaster -p 8787:8787
```

Pour vérifier le bon fonctionnement, lancer votre navigateur et saisissez l'adresse `http://localhost:8787/auth-sign-in`. Les information de connexion sont:

```
http://localhost:8787/
# Usernam : rst
# Password: rst
```

Nodes
----------

Ce dossier comporte le fichier `Dockerfile` ayant servir à la création de l'image `gnathan/rnodes`. Cette image contient une installation de R.

Pour générer l'image `rnodes`, il faut suivre les étapes suivantes:
```
    $ cd nodes
    $ docker build -t rnodes:v0.0.1 .
    $ docker run --name node -i rnodes
```

Le cluster
----------

Les commandes ci-dessus crée séparément les containers. Pour créer un réseau de containers, nous utilisons `docker-compose`.  Pour cela, il faut se mettre à la racine du projet et exécuter la commande `docker-compose up -d`. Cette commande crée plusieurs services dont un (1) container master et deux (2) containers worker et un réseau. Les adresses ip des trois containers sont:

```
172.18.0.11
172.18.0.12
172.18.0.13
```

Enfin, il est possible de télécharger des images du master et des workers sans avoir à les réconstruire comme indiqué plus haut. Pour cela, il faudra simplement éxécuter les commandes suivantes:

```
docker pull gnathan/rmaster:v0.0.1
docker pull gnathan/rnodes:v0.0.1
```
